# Parking System with Road Traffic Condition
- EE4216
- Internet Client-Server Computing
- 2018/19 Semester A
- Parking System with Road Traffic Condition - Parking System with Road Traffic Condition

# Installation
## Docker
Please refer to the [docker/readme.md](https://gitlab.com/roylau134/parking-system-with-road-traffic-condition/blob/master/docker/readme.md)

## Java - Maven
Please refer to the [maven/readme.md](https://gitlab.com/roylau134/parking-system-with-road-traffic-condition/blob/master/maven/readme.md)

# Start the app
## Web
https://www.psrtc.tk

## Restful API
https://www.psrtc.tk/api/  
https://www.psrtc.tk/api/status

## Kibana Ui (for internal use)
https://kibana.psrtc.tk/
- AC: elastic
- PW: changeme

## Elasticsearch Ui (for internal use)
https://elasticsearch.psrtc.tk/
- AC: elastic
- PW: changeme

# CI/CD
in order to use CI/CD, you need to merge the branch to Master branch.  
then the Build Pipline will auto build it

# DNS
[Freenom](https://my.freenom.com/domains.php)

# Server
[Digital Ocean](https://cloud.digitalocean.com/)