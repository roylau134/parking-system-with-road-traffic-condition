FROM nginx:1.10
MAINTAINER Roy Lau

RUN mkdir /etc/nginx/certs
COPY nginx/certificate.crt /etc/nginx/certs/certificate.crt
COPY nginx/private.key /etc/nginx/certs/private.key

ADD nginx/nginx.conf /etc/nginx/nginx.conf