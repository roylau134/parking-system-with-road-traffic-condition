# Docker Install guide
you need to ready a linux VM in order to process the following stage.
you can hold the VM by AWS or VMWare.

# Install Step
1. Linux ready
2. Install Docker container
3. Copy all file to VM
4. Use Docker Compose to start services

## Step 1: Linux ready
### VMware (local host PC)
> install VMware Workstation

> install [ubuntu](http://ubuntu.vxroutes.com/16.04.3/ubuntu-16.04.3-server-amd64.iso)

### AWS 

#### Linux - install SSH (Optional: for non-AWS or without SSH)
```sh
#apt-get update
apt-get update

# install ssh
sed -i '/cdrom/d' /etc/apt/sources.list
apt-get install -y openssh-server

# restart ssh
/etc/init.d/ssh restart

#show ip
ifconfig
```

### MacOS
Do not require special set up

## Step 2 - install Docker
### For linux
[Offical guide](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
```sh
#apt-get update
apt-get update

#install docker
apt-get install -y  \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
	
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"   
apt-get update
apt-get install -y docker-ce
apt-cache madison docker-ce

### install docker compose
curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

#test docker is ready
docker run hello-world
```

### For MacOS
install docker, link [here](https://store.docker.com/editions/community/docker-ce-desktop-mac)

## Step 3 - Copy file
You may use git download of ftp to upload those file.

### VMWare
Mount drive from VMWare Workstation
```sh
mkdir /mountDrive
/usr/bin/vmhgfs-fuse .host:/ /mountDrive/ -o subtype=vmhgfs-fuse,allow_other
```

You can create shell script so that you are not requied to type the command everytime.
```sh
#!/bin/bash
/usr/bin/vmhgfs-fuse .host:/ /mountDrive/ -o subtype=vmhgfs-fuse,allow_other
``` 

### MacOS
Do not require special set up

## Step 4 - start services
go to <root>/docker/ Use the following command to run the docker
```sh
docker-compose build
docker-compose up
```

## Useful Command
```sh
#delete all containers
docker rm $(docker ps -a -q)
#delete all images
docker rmi $(docker images -q)
#stop all docker container, then rebuild the image, then start the docker
docker-compose stop && docker-compose build && docker-compose up
```