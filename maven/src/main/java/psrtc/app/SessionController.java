/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psrtc.app;

import java.time.LocalDateTime;
import javax.servlet.http.HttpSession;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vanting
 */
@RestController
public class SessionController {

  @GetMapping(value = "/session", produces = MediaType.TEXT_PLAIN_VALUE)
  public String handler(HttpSession httpSession) {
      String sessionKey = "firstAccessTime";
      Object time = httpSession.getAttribute(sessionKey);
      if (time == null) {
          time = LocalDateTime.now();
          httpSession.setAttribute(sessionKey, time);
      }
      return "first access time : " + time+"\nsession id: "+httpSession.getId();
  }
}
