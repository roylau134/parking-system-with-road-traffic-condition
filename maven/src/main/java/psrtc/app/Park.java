package psrtc.app;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

public class Park {
    private String park_Id;
    private String name;
    private String address;
    private String contactNo;
    private String openingStatus;
    private double latitude;
    private double longitude;
    private JSONArray paymentMethods;
    private String website;
    private JSONArray privateCarVacancy;

    public Park(String pid, String name, String address,
        String contactNo, String openingStatus, double latitude,
        double longitude, String website, JSONArray paymentMethods, JSONArray privateCarVacancy){
        this.setPark_Id(pid);
        this.setName(name);
        this.setAddress(address);
        this.setContactNo(contactNo);
        this.setOpeningStatus(openingStatus);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setWebsite(website);
        this.setPaymentMethods(paymentMethods);
        this.setPrivateCarVacancy(privateCarVacancy);
    }

    public Park(String pid, String name, String address, String contactNo, String openingStatus, double latitude,
            double longitude, String website, JSONArray paymentMethods) {
        this.setPark_Id(pid);
        this.setName(name);
        this.setAddress(address);
        this.setContactNo(contactNo);
        this.setOpeningStatus(openingStatus);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setWebsite(website);
        this.setPaymentMethods(paymentMethods);
    }

    public Park(){
        
    }

    /**
     * @return the privateCarVacancy
     */
    public JSONArray getPrivateCarVacancy() {
        return privateCarVacancy;
    }

    /**
     * @param privateCarVacancy the privateCarVacancy to set
     */
    public void setPrivateCarVacancy(JSONArray privateCarVacancy) {
        this.privateCarVacancy = privateCarVacancy;
    }

    /**
     * @return the park_Id
     */
    public String getPark_Id() {
        return park_Id;
    }

    /**
     * @param park_Id the park_Id to set
     */
    public void setPark_Id(String park_Id) {
        this.park_Id = park_Id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the openingStatus
     */
    public String getOpeningStatus() {
        return openingStatus;
    }

    /**
     * @param openingStatus the openingStatus to set
     */
    public void setOpeningStatus(String openingStatus) {
        this.openingStatus = openingStatus;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the paymentMethods
     */
    public JSONArray getPaymentMethods() {
        return paymentMethods;
    }

    /**
     * @param paymentMethods the paymentMethods to set
     */
    public void setPaymentMethods(JSONArray paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website the website to set
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}