/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psrtc.app;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import javax.net.ssl.SSLContext;

import com.mashape.unirest.http.*;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api") // base URL for all handlers
public class RestfulController {

    Logger logger = LoggerFactory.getLogger(LoggingController.class);
    JSONObject parkObjArr[];

    @GetMapping("/carpark")
    public String getPark() {
        try {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true)
                    .build();

            CloseableHttpClient client = HttpClients.custom().setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

            Unirest.setHttpClient(client);

            HttpResponse<JsonNode> body1 = Unirest.get("https://api.data.gov.hk/v1/carpark-info-vacancy").asJson();
            int statusCode1 = body1.getStatus();
            logger.info("Status Code: " + Integer.toString(statusCode1));
            JsonNode responseBody1 = body1.getBody();
            JSONObject jsonObj1 = new JSONObject(responseBody1.toString());
            JSONArray array1 = jsonObj1.getJSONArray("results");

            HttpResponse<JsonNode> body2 = Unirest.get("https://api.data.gov.hk/v1/carpark-info-vacancy?data=vacancy")
                    .asJson();
            int statusCode2 = body2.getStatus();
            logger.info("Status Code: " + Integer.toString(statusCode2));
            JsonNode responseBody2 = body2.getBody();
            JSONObject jsonObj2 = new JSONObject(responseBody2.toString());
            JSONArray array2 = jsonObj2.getJSONArray("results");

            String content = "[";
            for (int i = 0; i < array1.length(); i++) {
                JSONObject park = new JSONObject(array1.get(i).toString());
                JSONObject vacancy = new JSONObject(array2.get(i).toString());
                JSONArray privateCarVacancy;
                try {
                    privateCarVacancy = new JSONArray(vacancy.get("privateCar").toString());
                } catch (JSONException e) {
                    privateCarVacancy = new JSONArray();
                }
                String pid = park.get("park_Id").toString();
                String name = park.get("name").toString();
                String address = park.get("displayAddress").toString();
                // try {
                //     address = new JSONObject(park.get("displayaddress").toString());
                // } catch (JSONException e) {
                //     address = new JSONObject();
                // }
                String contactNo = park.get("contactNo").toString();
                String openingStatus = park.get("opening_status").toString();
                Double latitude = Double.valueOf(park.get("latitude").toString());
                Double longitude = Double.valueOf(park.get("longitude").toString());
                String website = park.get("website").toString();
                JSONArray paymentMethods;
                try {
                    paymentMethods = new JSONArray(park.get("paymentMethods").toString());
                } catch (JSONException e) {
                    paymentMethods = new JSONArray();
                }
                Park temp = new Park(pid, name, address, contactNo, openingStatus, latitude, longitude, website,
                        paymentMethods, privateCarVacancy);
                content += temp.getJson();

                content += ",";
            }
            try {
                Unirest.shutdown();
            } catch (IOException e) {
                e.printStackTrace();
            }
            content = content.substring(0, content.length() - 1);
            content += "]";
            return content;
        } catch (UnirestException e) {
            logger.info("ERROR in fetching the API", e);
            return "Unirest Error";
        } catch (KeyStoreException e) {
            logger.info("KeyStoreException", e);
            return "KeyStoreException";
        } catch (KeyManagementException e) {
            logger.info("KeyManagementException", e);
            return "KeyManagementException";
        } catch (NoSuchAlgorithmException e) {
            logger.info("NoSuchAlgorithmException", e);
            return "NoSuchAlgorithmException";
        }
    }

    @GetMapping("/road")
    public String getRoad() throws Exception {
        String urlS = "http://static.data.gov.hk/td/traffic-snapshot-images/code/Traffic_Camera_Locations_En.xml";

        URL url = new URL(urlS);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/xml");

        InputStream inputStream = connection.getInputStream();

        try (Scanner scanner = new Scanner(inputStream)) {//auto relaise the resource after complete
            String stringXml = scanner.useDelimiter("\\Z").next();
            JSONObject xmlJSONObj = XML.toJSONObject(stringXml);
            return xmlJSONObj.toString();
        } catch (JSONException je) {
            return je.toString();
        }
    }

    @GetMapping("/status") // map to /api/status
    public String status() {
        return "Working - use /info1 to check the getAPI func";
    }
}
