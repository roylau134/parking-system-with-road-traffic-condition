# Maven Install
```sh
# current location should upder {project directory}/maven
mvn clean package
mvn spring-boot:run

#one line statment, same as above
mvn clean package && mvn spring-boot:run
```