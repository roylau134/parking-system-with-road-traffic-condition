
var map;
var myLatLng;

var app = new Vue({
    el: '#app',
    data: {
        source: '',
        dest: '',
    },
    row: [
        { name: '', displayAddress: '', latitude: '', longitude: '' },
    ],
    road: [{ name: '', displayAddress: '', latitude: '', longitude: '' }]
    ,
    mounted: function () {
        //fetch car park data
        fetch('/api/carpark').then((Response) => {
            return Response.json();
        }).then((park) => {
            this.rows = park;
        }).then(() => {
            initializeParkMarking();
        })
        //fetch road data
        fetch('/api/road').then((Response) => {
            return Response.json();
        }).then((road) => {
            this.roads = road;
        }).then(() => {
            initializeroadsMarking();
        }).then(() => {
            window.setInterval(initializeParkMarking,300000);
            window.setInterval(initializeroadsMarking,300000);
        })
    }
});



function initMap() {
    
    //create map
    map = new google.maps.Map(document.getElementById('googleMap'), {
        center: { lat: 22.3964, lng: 114.1095 },
        zoom: 11
    });
    var trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(map);

    //self location
    if (navigator.geolocation) { 
        navigator.geolocation.getCurrentPosition(function (position) { 
            //locate to current position
            console.log("Self Location Success!")
            var pos = { lat: position.coords.latitude, lng: position.coords.longitude }; 
            myLatLng = new google.maps.LatLng(pos.lat,pos.lng);
            map.setZoom(16);
            map.setCenter(pos);
        }, 
        function () { 
            //if fail locate to cityu
            alert("Self Location Fail !\nNow locate to CityU");
            var pos = { lat: 22.337, lng: 114.172}; 
            myLatLng = new google.maps.LatLng(pos.lat,pos.lng);
            map.setZoom(16);
            map.setCenter(pos);
        }); 
    } else {         // Browser doesn't support Geolocation
        alert("Self Location Not Supported");
    }
}

function findParking(latlng)
{
    function rad(x) {
        return x*Math.PI/180;
    }
    
    var R = 6371; // radius of earth in km
    var distances = [];
    var closest = -1;
    for( i=0; i< app.rows.length; i++ ) {
        var mlat = app.rows[i].latitude;
        var mlng =  app.rows[i].longitude;
        var dLat  = rad(mlat - latlng.lat());
        var dLong = rad(mlng - latlng.lng());
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(latlng.lat())) * Math.cos(rad(latlng.lat())) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        distances[i] = d;
        if ( closest == -1 || (d < distances[closest] && app.rows[i].openingStatus == "OPEN" && app.rows[i].privateCarVacancy.myArrayList[0].map["vacancy"] >= 1)) {
            closest = i;
        }
        console.log(closest);
    }

    if(closest != -1){
        console.log("start to route");
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        directionsDisplay.setOptions( { suppressMarkers: true } );
        directionsDisplay.setMap(map);

        carParkLatLng = new google.maps.LatLng(app.rows[closest].latitude,app.rows[closest].longitude);

        directionsService.route({
            origin: myLatLng,
            destination: carParkLatLng,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
            directionsDisplay.setDirections(response);
            } else {
            alert('Directions request failed due to ' + status);
            }
        });
    }
}
//Park marking
var parkmark = [];
function initializeParkMarking() {
    parkmark = [];
    var infowindow = new google.maps.InfoWindow(); /* SINGLE */
    var parkImage = 'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png';

    function placeMarker(i) {
        var latLng = new google.maps.LatLng(app.rows[i].latitude, app.rows[i].longitude);
        var marker = new google.maps.Marker({
            position: latLng,
            icon: parkImage,
            map: map
        });
        parkmark.push(marker);
        //Payment method loop
        var paym = "";
        for (var j = 0; j < app.rows[i].paymentMethods.myArrayList.length; j++) {
            paym += app.rows[i].paymentMethods.myArrayList[j] + " ";
        }
        if (paym === "") {
            paym = "No supported information";
        }
        //Open status Check
        var openStatus = "";
        if (app.rows[i].openingStatus == "OPEN") {
            openStatus = "<font color='green'>OPEN</font>";
        } else {
            openStatus = "<font color='red'>CLOSE</font>";
        }
        //Indo Window display content 
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.close(); // Close previously opened infowindow
            infowindow.setContent("<div id='infowindow'>" +
                "<h3>" + app.rows[i].name + "</h3>" +
                "<a>Address: " + app.rows[i].address + "</a><br>" +
                "<a>Contact number: " + app.rows[i].contactNo + "</a><br>" +
                "<a>Open status: <strong>" + openStatus + "</strong></a><br>" +
                "<a>Payment method: " + paym + "</a><br>" + //loop PM
                "<a>Vacancy type: <strong>" + app.rows[i].privateCarVacancy.myArrayList[0].map["vacancy_type"] + "</strong></a><br>" +
                "<a>Private car vacancy: <strong>" + app.rows[i].privateCarVacancy.myArrayList[0].map["vacancy"] + "</strong></a><br>" +
                "<a>Lastupdate: " + app.rows[i].privateCarVacancy.myArrayList[0].map["lastupdate"] + "</a><br>" +
                "</div>");
            infowindow.open(map, marker);
            map.setZoom(15);
            map.setCenter(marker.getPosition());
        });
    }

    // ITERATE ALL LOCATIONS
    // Don't create functions inside for loops
    // therefore refer to a previously created function
    // and pass your iterating location as argument value:
    for (var i = 0; i < app.rows.length; i++) {
        placeMarker(i);
    }

    findParking(myLatLng);
}
//Road marking
var roadmark = [];
function initializeroadsMarking() {
    roadmark = [];
    var infowindow = new google.maps.InfoWindow(); /* SINGLE */
    var image = {
    url:'https://s3.envato.com/files/246506605/Thumnail.png',
    size: new google.maps.Size(80, 80),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 10),
    scaledSize: new google.maps.Size(40, 40)
  };

    function placeroadsMarker(i) {
        var latLng = new google.maps.LatLng(app.roads["image-list"].image[i].latitude, app.roads["image-list"].image[i].longitude);
        var marker = new google.maps.Marker({
            position: latLng,
            map: null,
            icon: image
        });
        roadmark.push(marker);
        //Indo Window display content 
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.close(); // Close previously opened infowindow
            infowindow.setContent(
            "<div id='infowindow'><h3>" + 
            app.roads["image-list"].image[i].description+"</h3>"+
            "<style> #infowindow{width: 350px; height: 350px;} #roadIMG{width: 320px; height: 240px;}</style>"+
            "<img id='roadIMG' src="+app.roads["image-list"].image[i].url+">"+
            "</div>");
            infowindow.open(map, marker);
            map.setZoom(16);
            map.setCenter(marker.getPosition());
        });
    }
    // ITERATE ALL LOCATIONS
    // Don't create functions inside for loops
    // therefore refer to a previously created function
    // and pass your iterating location as argument value:
    for (var i = 0; i < app.roads["image-list"].image.length; i++) {
        placeroadsMarker(i);
    }
}

var tCarPark = true;
var tRoadCam = false;
function ToggleCarPark(){
    console.log("ToggleCarPark");
    if(tCarPark){
        setMapOnAll(null,parkmark)
    }else{
        setMapOnAll(map,parkmark)
    }
    tCarPark = !tCarPark;
}

function ToggleRoadCam(){
    console.log("ToggleRoadCam");
    if(tRoadCam){
        setMapOnAll(null,roadmark)
    }else{
        setMapOnAll(map,roadmark)
    }
    tRoadCam = !tRoadCam;
}
function setMapOnAll(map,tmarker) {
    for (var i = 0; i < tmarker.length; i++) {
        tmarker[i].setMap(map);
    }
}